# Valheim Mods

Please note that this will overwrite files in your BepInEx, doorstop_libs, unstripped_corlib folders, other mods that aren't included here shouldn't get overwritten, probably

## Follow these steps ##
- cd into Valheim root directory (ex. `C:\Program Files (x86)\Steam\steamapps\common\Valheim`)
- git init
- git remote add origin https://gitlab.com/ekauz/valheim-mods.git
- git fetch origin
- git checkout origin/master -ft
